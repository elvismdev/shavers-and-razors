<?php
/*
Plugin Name: Amazon Link Extra - Exported Templates
Plugin URI: http://www.houseindorset.co.uk/plugins/amazon-link/
Description: Templates Exported from Amazon Link on May 16, 2014, 6:58 am
Version: 1.0
Author: Amazon Link User
Author URI: http://shaversandrazors.dev
*/

function alx_shaver_sand_razors_default_templates ($templates) {
 $templates['add to cart'] = 
  array(   'Name' => 'Add To Cart',
   'Description' => 'Buy From Amazon Button',
   'Content' => htmlspecialchars ('
<form method="GET" action="http://www.amazon.%TLD%/gp/aws/cart/add.html">
 <input type="hidden" name="AssociateTag" value="%TAG%"/>
 <input type="hidden" name="SubscriptionId" value="%PUB_KEY%"/>
 <input type="hidden" name="ASIN.1" value="%ASIN%"/>
 <input type="hidden" name="Quantity.1" value="1"/>
 <input type="image" name="add" value="Buy from Amazon.%TLD%" border="0" alt="Buy from Amazon.%TLD%" src="%BUY_BUTTON%">
</form>'),
   'Version' => '2',
   'Notice' => 'Remove line breaks',
   'Type' => 'Product',
   'Preview_Off' => 0,
  );
 $templates['banner easy'] = 
  array(   'Name' => 'Banner Easy',
   'Description' => 'Easy Banner (468x60)',
   'Content' => htmlspecialchars ('
<iframe src="http://%RCM%/e/cm?t=%TAG%&o=%MPLACE_ID%&p=26&l=ez&f=ifr&f=ifr" width="468" height="60" scrolling="no" marginwidth="0" marginheight="0" border="0" frameborder="0" style="border:none;">
</iframe>'),
   'Version' => '1',
   'Notice' => '',
   'Type' => 'No ASIN',
   'Preview_Off' => 0,
  );
 $templates['carousel'] = 
  array(   'Name' => 'Carousel',
   'Description' => 'Amazon Carousel Widget (limited locales)',
   'Content' => htmlspecialchars ('
<script type=\'text/javascript\'>
var amzn_wdgt={widget:\'Carousel\'};
amzn_wdgt.tag=\'%TAG%\';
amzn_wdgt.widgetType=\'ASINList\';
amzn_wdgt.ASIN=\'%ASINs%\';
amzn_wdgt.title=\'%TEXT%\';
amzn_wdgt.marketPlace=\'%MPLACE%\';
amzn_wdgt.width=\'600\';
amzn_wdgt.height=\'200\';
</script>
<script type=\'text/javascript\' src=\'http://wms.assoc-amazon.%TLD%/20070822/%MPLACE%/js/swfobject_1_5.js\'>
</script>'),
   'Version' => '1',
   'Notice' => '',
   'Type' => 'Multi',
   'Preview_Off' => 0,
  );
 $templates['homepage'] = 
  array(   'Name' => 'Homepage',
   'Description' => 'Use this to place products in the home page',
   'Type' => 'Product',
   'Preview_Off' => NULL,
   'Content' => htmlspecialchars ('<div class="al_found%FOUND%">
 <div class="amazon_prod">
  <div class="amazon_img_container">
   %LINK_OPEN%<img class="%IMAGE_CLASS%" src="%THUMB%">%LINK_CLOSE%
  </div>
  <div class="amazon_text_container">
   <p>%LINK_OPEN%%TITLE%%LINK_CLOSE%</p>
   <div class="amazon_details">
     <p>by %MANUFACTURER%<br />
        Rating: %RATING%<br />
        <b>Price: <span class="amazon_price">%PRICE%</span></b>
    </p>
   </div>
  </div>
 </div>
</div>'),
   'Version' => 1,
   'Notice' => 'New Template',
  );
 $templates['iframe image'] = 
  array(   'Name' => 'Iframe Image',
   'Description' => 'Standard Amazon Image Link',
   'Content' => htmlspecialchars ('
<iframe src="http://%RCM%/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=%TAG%&o=%MPLACE_ID%&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=%ASIN%" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>
'),
   'Type' => 'Product',
   'Version' => '1',
   'Notice' => '',
   'Preview_Off' => 0,
  );
 $templates['image'] = 
  array(   'Name' => 'Image',
   'Description' => 'Localised Image Link',
   'Content' => htmlspecialchars ('
<div class="al_found%FOUND%">
 %LINK_OPEN%<img alt="%TITLE%" title="%TITLE%" src="%IMAGE%" class="%IMAGE_CLASS%">%LINK_CLOSE%
<img src="http://www.assoc-amazon.%TLD%/e/ir?t=%TAG%&l=as2&o=%MPLACE_ID%&a=%ASIN%" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />
</div>
'),
   'Type' => 'Product',
   'Version' => '2',
   'Notice' => 'Add impression tracking',
   'Preview_Off' => 0,
  );
 $templates['mp3 clips'] = 
  array(   'Name' => 'MP3 Clips',
   'Description' => 'Amazon MP3 Clips Widget (limited locales)',
   'Content' => htmlspecialchars ('
<script type=\'text/javascript\'>
var amzn_wdgt={widget:\'MP3Clips\'};
amzn_wdgt.tag=\'%TAG%\';
amzn_wdgt.widgetType=\'ASINList\';
amzn_wdgt.ASIN=\'%ASINS%\';
amzn_wdgt.title=\'%TEXT%\';
amzn_wdgt.width=\'250\';
amzn_wdgt.height=\'250\';
amzn_wdgt.shuffleTracks=\'True\';
amzn_wdgt.marketPlace=\'%MPLACE%\';
</script>
<script type=\'text/javascript\' src=\'http://wms.assoc-amazon.%TLD%/20070822/%MPLACE%/js/swfobject_1_5.js\'>
</script>'),
   'Version' => '1',
   'Notice' => '',
   'Type' => 'Multi',
   'Preview_Off' => 0,
  );
 $templates['multinational'] = 
  array(   'Name' => 'Multinational',
   'Description' => 'Example Multinational Template',
   'Content' => htmlspecialchars ('
<div class="amazon_prod">
 <div class="amazon_img_container">
  %LINK_OPEN%<img class="%IMAGE_CLASS%" src="%THUMB%">%LINK_CLOSE%
 </div>
 <div class="amazon_text_container">
  <p>%LINK_OPEN%%TITLE%%LINK_CLOSE%</p>
  <div class="amazon_details">
   <p>by %ARTIST% [%MANUFACTURER%]<br />
    <b>Price: <span class="amazon_price">
     <span class="al_found%FOUND%uk#">%LINK_OPEN%uk# <img style="height:10px" src="%FLAG%uk#"> %PRICE%uk#%LINK_CLOSE%</span>
     <span class="al_found%FOUND%fr#">%LINK_OPEN%FR# <img style="height:10px" src="%FLAG%fr#"> %PRICE%FR#%LINK_CLOSE%</span>
     <span class="al_found%FOUND%de#">%LINK_OPEN%de# <img style="height:10px" src="%FLAG%de#"> %PRICE%DE#%LINK_CLOSE%</span>
     <span class="al_found%FOUND%es#">%LINK_OPEN%es# <img style="height:10px" src="%FLAG%es#"> %PRICE%es#%LINK_CLOSE%</span>
    </b>
   </p>
  </div>
 </div>
</div>
<img src="http://www.assoc-amazon.%TLD%/e/ir?t=%TAG%&l=as2&o=%MPLACE_ID%&a=%ASIN%" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />
'),
   'Type' => 'Product',
   'Version' => '3',
   'Notice' => 'Use style to modify height',
   'Preview_Off' => 0,
  );
 $templates['my favourites'] = 
  array(   'Name' => 'My Favourites',
   'Description' => 'Amazon My Favourites Widget (limited locales)',
   'Content' => htmlspecialchars ('
<script type=\'text/javascript\'>
var amzn_wdgt={widget:\'MyFavorites\'};
amzn_wdgt.tag=\'%TAG%\';
amzn_wdgt.columns=\'1\';
amzn_wdgt.rows=\'3\';
amzn_wdgt.title=\'%TEXT%\';
amzn_wdgt.width=\'250\';
amzn_wdgt.ASIN=\'%ASINS%\';
amzn_wdgt.showImage=\'True\';
amzn_wdgt.showPrice=\'True\';
amzn_wdgt.showRating=\'True\';
amzn_wdgt.design=\'5\';
amzn_wdgt.colorTheme=\'White\';
amzn_wdgt.headerTextColor=\'#FFFFFF\';
amzn_wdgt.marketPlace=\'%MPLACE%\';
</script>
<script type=\'text/javascript\' src=\'http://wms.assoc-amazon.%TLD%/20070822/%MPLACE%/js/AmazonWidgets.js\'>
</script>'),
   'Version' => '1',
   'Notice' => '',
   'Type' => 'Multi',
   'Preview_Off' => 0,
  );
 $templates['preview script'] = 
  array(   'Name' => 'Preview Script',
   'Description' => 'Add Amazon Preview Pop-up script (limited locales)',
   'Content' => htmlspecialchars ('
<script type="text/javascript" src="http://wms.assoc-amazon.%TLD%/20070822/%MPLACE%/js/link-enhancer-common.js?tag=%TAG%">
</script>
<noscript>
    <img src="http://wms.assoc-amazon.%TLD%/20070822/%MPLACE%/img/noscript.gif?tag=%TAG%" alt="" />
</noscript>'),
   'Version' => '1',
   'Notice' => '',
   'Type' => 'No ASIN',
   'Preview_Off' => 1,
  );
 $templates['thumbnail'] = 
  array(   'Name' => 'Thumbnail',
   'Description' => 'Localised Thumb Link',
   'Content' => htmlspecialchars ('
<div class="al_found%FOUND%">
 %LINK_OPEN%<img alt="%TITLE%" title="%TITLE%" src="%THUMB%" class="%IMAGE_CLASS%">%LINK_CLOSE%
<img src="http://www.assoc-amazon.%TLD%/e/ir?t=%TAG%&l=as2&o=%MPLACE_ID%&a=%ASIN%" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />
</div>'),
   'Type' => 'Product',
   'Version' => '2',
   'Notice' => 'Add impression tracking',
   'Preview_Off' => 0,
  );
 $templates['wishlist'] = 
  array(   'Name' => 'Wishlist',
   'Description' => 'Used to generate the wishlist',
   'Content' => htmlspecialchars ('
<div class="al_found%FOUND%">
 <div class="amazon_prod">
  <div class="amazon_img_container">
   %LINK_OPEN%<img class="%IMAGE_CLASS%" src="%THUMB%">%LINK_CLOSE%
  </div>
  <div class="amazon_text_container">
   <p>%LINK_OPEN%%TITLE%%LINK_CLOSE%</p>
   <div class="amazon_details">
     <p>by %ARTIST% [%MANUFACTURER%]<br />
        Rank/Rating: %RANK%/%RATING%<br />
        <b>Price: <span class="amazon_price">%PRICE%</span></b>
    </p>
   </div>
  </div>
 </div>
<img src="http://www.assoc-amazon.%TLD%/e/ir?t=%TAG%&l=as2&o=%MPLACE_ID%&a=%ASIN%" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />
</div>'),
   'Type' => 'Product',
   'Version' => '2',
   'Notice' => 'Add impression tracking',
   'Preview_Off' => 0,
  );
  return $templates;
}
add_filter( 'amazon_link_default_templates', 'alx_shaver_sand_razors_default_templates');
?>