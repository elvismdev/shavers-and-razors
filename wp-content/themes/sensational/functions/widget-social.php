<?php
/*
Plugin Name: Social Profile Icons
*/

//Widget Registration.
 
function mythemeshop_load_widget() {

	register_widget( 'Social_Profile_Icons_Widget' );

}
class Social_Profile_Icons_Widget extends WP_Widget {

	/**
	 * Default widget values.
	 *
	 * @var array
	 */
	protected $defaults;

	/**
	 * Default widget values.
	 *
	 * @var array
	 */
	protected $sizes;

	/**
	 * Default widget values.
	 *
	 * @var array
	 */
	protected $profiles;

	/**
	 * Constructor method.
	 *
	 * Set some global values and create widget.
	 */
	function __construct() {

		/**
		 * Default widget option values.
		 */
		$this->defaults = array(
			'title'					 => '',
			'new_window'			 => 0,
			'size'					 => 32,
			'border_radius'			 => 3,
			'background_color'		 => '#999999',
			'background_color_hover' => '#666666',
			'dribbble'				 => '',
			'email'				 => '',
			'facebook'				 => '',
			'gplus'					 => '',
			'linkedin'				 => '',
			'pinterest'				 => '',
			'rss'					 => '',
			'stumbleupon'				 => '',
			'twitter'				 => '',
			'youtube'				 => '',
		);

		/**
		 * Icon sizes.
		 */
		$this->sizes = array( '32' );

		/**
		 * Social profile choices.
		 */
		$this->profiles = array(
			'dribbble' => array(
				'label'	  => __( 'Dribbble URI', 'mythemeshop' ),
				'pattern' => '<li class="social-dribbble"><a title="Dribbble" href="%s" %s>Dribbble</a></li>',
				'background_positions' => array(
					'32' => '0 0',
				)
			),
			'email' => array(
				'label'	  => __( 'Email URI', 'mythemeshop' ),
				'pattern' => '<li class="social-email"><a title="Email" href="%s" %s>Email</a></li>',
				'background_positions' => array(
					'32' => '-32px 0',
				)
			),
			'facebook' => array(
				'label'	  => __( 'Facebook URI', 'mythemeshop' ),
				'pattern' => '<li class="social-facebook"><a title="Facebook" href="%s" %s>Facebook</a></li>',
				'background_positions' => array(
					'32' => '-64px 0',
				)
			),
			'gplus' => array(
				'label'	  => __( 'Google+ URI', 'mythemeshop' ),
				'pattern' => '<li class="social-gplus"><a title="Google+" href="%s" %s>Google+</a></li>',
				'background_positions' => array(
					'32' => '-96px 0',
				)
			),
			'linkedin' => array(
				'label'	  => __( 'Linkedin URI', 'mythemeshop' ),
				'pattern' => '<li class="social-linkedin"><a title="LinkedIn" href="%s" %s>Linkedin</a></li>',
				'background_positions' => array(
					'32' => '-128px 0',
				)
			),
			'pinterest' => array(
				'label'	  => __( 'Pinterest URI', 'mythemeshop' ),
				'pattern' => '<li class="social-pinterest"><a title="Pinterest" href="%s" %s>Pinterest</a></li>',
				'background_positions' => array(
					'32' => '-160px 0',
				)
			),
			'rss' => array(
				'label'	  => __( 'RSS URI', 'mythemeshop' ),
				'pattern' => '<li class="social-rss"><a title="RSS" href="%s" %s>RSS</a></li>',
				'background_positions' => array(
					'32' => '-192px 0',
				)
			),
			'stumbleupon' => array(
				'label'	  => __( 'StumbleUpon URI', 'mythemeshop' ),
				'pattern' => '<li class="social-stumbleupon"><a title="StumbleUpon" href="%s" %s>StumbleUpon</a></li>',
				'background_positions' => array(
					'32' => '-224px 0',
				)
			),
			'twitter' => array(
				'label'	  => __( 'Twitter URI', 'mythemeshop' ),
				'pattern' => '<li class="social-twitter"><a title="Twitter" href="%s" %s>Twitter</a></li>',
				'background_positions' => array(
					'32' => '-256px 0',
				)
			),
			'youtube' => array(
				'label'	  => __( 'YouTube URI', 'mythemeshop' ),
				'pattern' => '<li class="social-youtube"><a title="YouTube" href="%s" %s>YouTube</a></li>',
				'background_positions' => array(
					'32' => '-288px 0',
				)
			),
		);

		$widget_ops = array(
			'classname'	  => 'social-profile-icons',
			'description' => __( 'Show profile icons.', 'mythemeshop' ),
		);

		$control_ops = array(
			'id_base' => 'social-profile-icons',
			#'width'   => 505,
			#'height'  => 350,
		);

		$this->WP_Widget( 'social-profile-icons', __( 'MyThemeShop: Social Profile Icons', 'mythemeshop' ), $widget_ops, $control_ops );

		/** Load CSS in <head> */
		add_action( 'wp_head', array( $this, 'css' ) );

	}

	/**
	 * Widget Form.
	 *
	 * Outputs the widget form that allows users to control the output of the widget.
	 *
	 */
	function form( $instance ) {

		/** Merge with defaults */
		$instance = wp_parse_args( (array) $instance, $this->defaults );
		?>

		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'mythemeshop' ); ?></label> <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>" /></p>

		<p><label><input id="<?php echo $this->get_field_id( 'new_window' ); ?>" type="checkbox" name="<?php echo $this->get_field_name( 'new_window' ); ?>" value="1" <?php checked( 1, $instance['new_window'] ); ?>/> <?php esc_html_e( 'Open links in new window?', 'mythemeshop' ); ?></label></p>

		<p><label for="<?php echo $this->get_field_id( 'border_radius' ); ?>"><?php _e( 'Icon Border Radius:', 'mythemeshop' ); ?></label> <input id="<?php echo $this->get_field_id( 'border_radius' ); ?>" name="<?php echo $this->get_field_name( 'border_radius' ); ?>" type="text" value="<?php echo esc_attr( $instance['border_radius'] ); ?>" size="3" />px</p>

		<p><label for="<?php echo $this->get_field_id( 'background_color' ); ?>"><?php _e( 'Icon Color:', 'mythemeshop' ); ?></label> <input id="<?php echo $this->get_field_id( 'background_color' ); ?>" name="<?php echo $this->get_field_name( 'background_color' ); ?>" type="text" value="<?php echo esc_attr( $instance['background_color'] ); ?>" size="8" /></p>

		<p><label for="<?php echo $this->get_field_id( 'background_color_hover' ); ?>"><?php _e( 'Hover Color:', 'mythemeshop' ); ?></label> <input id="<?php echo $this->get_field_id( 'background_color_hover' ); ?>" name="<?php echo $this->get_field_name( 'background_color_hover' ); ?>" type="text" value="<?php echo esc_attr( $instance['background_color_hover'] ); ?>" size="8" /></p>

		<hr style="background: #ccc; border: 0; height: 1px; margin: 20px 0;" />

		<?php
		foreach ( (array) $this->profiles as $profile => $data ) {

			printf( '<p><label for="%s">%s:</label>', esc_attr( $this->get_field_id( $profile ) ), esc_attr( $data['label'] ) );
			printf( '<input type="text" id="%s" class="widefat" name="%s" value="%s" /></p>', esc_attr( $this->get_field_id( $profile ) ), esc_attr( $this->get_field_name( $profile ) ), esc_url( $instance[$profile] ) );

		}

	}

	/**
	 * Form validation and sanitization.
	 *
	 * Runs when you save the widget form. Allows you to validate or sanitize widget options before they are saved.
	 *
	 */
	function update( $newinstance, $oldinstance ) {

		foreach ( $newinstance as $key => $value ) {

			/** Border radius must not be empty, must be a digit */
			if ( 'border_radius' == $key && ( '' == $value || ! ctype_digit( $value ) ) ) {
				$newinstance[$key] = 0;
			}

			/** Validate hex code colors */
			elseif ( strpos( $key, '_color' ) && 0 == preg_match( '/^#(([a-fA-F0-9]{3}$)|([a-fA-F0-9]{6}$))/', $value ) ) {
				$newinstance[$key] = $oldinstance[$key];
			}

			/** Sanitize Profile URIs */
			elseif ( array_key_exists( $key, (array) $this->profiles ) ) {
				$newinstance[$key] = esc_url( $newinstance[$key] );
			}

		}

		return $newinstance;

	}

	/**
	 * Widget Output.
	 *
	 * Outputs the actual widget on the front-end based on the widget options the user selected.
	 *
	 */
	function widget( $args, $instance ) {

		extract( $args );

		/** Merge with defaults */
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		echo $before_widget;

			if ( ! empty( $instance['title'] ) )
				echo $before_title . apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base ) . $after_title;

			$output = '';

			$new_window = $instance['new_window'] ? 'target="_blank"' : '';

			foreach ( (array) $this->profiles as $profile => $data ) {
				if ( ! empty( $instance[$profile] ) )
					$output .= sprintf( $data['pattern'], esc_url( $instance[$profile] ), $new_window );
			}

			if ( $output )
				printf( '<div class="social-profile-icons"><ul class="%s">%s</ul></div>', '',$output );

		echo $after_widget;

	}

	/**
	 * Custom CSS.
	 *
	 * Outputs custom CSS to control the look of the icons.
	 */
	function css() {

		/** Pull widget settings, merge with defaults */
		$all_instances = $this->get_settings();
		$instance = wp_parse_args( $all_instances[$this->number], $this->defaults );

		/** The image locations */
		$imgs = array(
			'32' => get_template_directory_uri(). '/images/sprite_32x32.png',
		);

		/** The CSS to output */
		$css = '.social-profile-icons {
			overflow: hidden;
		}
		.social-profile-icons .alignleft, .social-profile-icons .alignright {
			margin: 0; padding: 0;
		}
		.social-profile-icons ul li {
			background: none !important;
			border: none !important;
			float: left;
			list-style-type: none !important;
			margin: 0 5px 10px !important;
			padding: 0 !important;
		}
		.social-profile-icons ul li a,
		.social-profile-icons ul li a:hover {
			background: ' . $instance['background_color'] . ' url(' . $imgs[$instance['size']] . ') no-repeat;
			-moz-border-radius: ' . $instance['border_radius'] . 'px
			-webkit-border-radius: ' . $instance['border_radius'] . 'px;
			border-radius: ' . $instance['border_radius'] . 'px;
			display: block;
			height: ' . $instance['size'] . 'px;
			overflow: hidden;
			text-indent: -999px;
			width: ' . $instance['size'] . 'px;
			-webkit-transition: all 0.25s linear;
			-moz-transition: all 0.25s linear;
			transition: all 0.25s linear;
		}

		.social-profile-icons ul li a:hover {
			background-color: ' . $instance['background_color_hover'] . ';
		}';

		/** Individual Profile button styles */
		foreach ( (array) $this->profiles as $profile => $data ) {

			if ( ! $instance[$profile] )
				continue;

			$css .= '.social-profile-icons ul li.social-' . $profile . ' a,
			.social-profile-icons ul li.social-' . $profile . ' a:hover {
				background-position: ' . $data['background_positions'][$instance['size']] . ';
			}';

		}

		/** Echo the CSS */
		echo '<style type="text/css" media="screen">' . $css . '</style>';

	}

}

add_action( 'widgets_init', 'mythemeshop_load_widget' );