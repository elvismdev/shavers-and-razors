<?php

/*-------------------------------------------------

	Plugin Name: MyThemeShop Twitter and RSS Count
	Description: A widget that shows twitter follower and RSS Subscriber count
	Version: 1.0

---------------------------------------------------*/


class mts_Subscribers extends WP_Widget {

	function mts_Subscribers() {	
	$widget_ops = array( 'classname' => 'subscribercount-widget', 'description' => __('A widget shows twitter follower and RSS Subscriber count', 'framework') );
		$this->WP_Widget( 'subscribers_widget', __('MyThemeShop: Twitter & RSS Count', 'framework'), $widget_ops);
	}
	
	function form($instance) {
	
		
		$instance = wp_parse_args( (array) $instance, array('title' => 'My Subscribers', 'rss' => '', 'twitter' => '', 'page_id' => '') );

        $title = esc_attr($instance['title']);
        $rss = $instance['rss'];
		$twitter = $instance['twitter'];
        $page_id = $instance['page_id'];

?>
		<p>
            <label for="<?php echo $this->get_field_id('title'); ?>">
               <?php _e('Title:', 'mythemeshop'); ?>
            </label>
                <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>

		<p>
            <label for="<?php echo $this->get_field_id('rss'); ?>">
               <?php _e('RSS URL:', 'mythemeshop'); ?>
            </label>
                <input class="widefat" id="<?php echo $this->get_field_id('rss'); ?>" name="<?php echo $this->get_field_name('rss'); ?>" type="text" value="<?php echo $rss; ?>" />
                
        </p>

		<p>
            <label for="<?php echo $this->get_field_id('twitter'); ?>">
               <?php _e('Twitter Username:', 'mythemeshop'); ?>
            </label>
                <input class="widefat" id="<?php echo $this->get_field_id('twitter'); ?>" name="<?php echo $this->get_field_name('twitter'); ?>" type="text" value="<?php echo $twitter; ?>" />
                
        </p>
        
        <p>
            <label for="<?php echo $this->get_field_id('page_id'); ?>">
               <?php _e('Facebook Page ID:', 'mythemeshop'); ?>
            </label>
                <input class="widefat" id="<?php echo $this->get_field_id('page_id'); ?>" name="<?php echo $this->get_field_name('page_id'); ?>" type="text" value="<?php echo $page_id; ?>" />
                
        </p>


<?php
    }

	function update($new_instance, $old_instance) {
        $instance=$old_instance;

        $instance['title'] = strip_tags($new_instance['title']);
        $instance['rss'] = $new_instance['rss'];
        $instance['twitter'] = $new_instance['twitter'];
        $instance['page_id'] = $new_instance['page_id'];
        return $instance;

    }

	function widget($args, $instance) {
	
		extract($args);

		$title = apply_filters('widget_title', $instance['title']);
		if ( empty($title) ) $title = false;

        $rss = $instance['rss'];
		$twitter = $instance['twitter'];
        $page_id = $instance['page_id'];
		
		
        echo $before_widget;
		
		if($title){
			echo $before_title;
			echo $title; 
			echo $after_title;
		}
		
        ?>
        
        <div class="subscribers-widget">
        
        <?php
        if( !empty($page_id) ):
        //facebook code
		$xml = @simplexml_load_file("http://api.facebook.com/restserver.php?method=facebook.fql.query&query=SELECT%20fan_count%20FROM%20page%20WHERE%20page_id=$page_id") or die ("a lot");
		$fans = $xml->page->fan_count;
        
        ?>
        
        <div class="subscriber-wrap">
                
            <a href="<?php echo "http://www.facebook.com/$page_id"; ?>" class="subscriber-icon"><img src="<?php echo get_bloginfo( 'template_url' ); ?>/images/facebook.png" alt="Facebook" width="48" height="48" /></a>
            
            <span class="subscribes"><?php echo $fans; ?></span>
            <span class="subscribes-text"><?php _e('Fans', 'mythemeshop'); ?></span>
                                        
        </div>
        
        <?php
        
        endif;
        
        ?>
        
        <?php
        
        if( !empty($twitter) ):
        
        $url = "http://twitter.com/users/show/" . $twitter;
        $response = file_get_contents ( $url );
        $t_profile = new SimpleXMLElement ( $response );
        $count = $t_profile->followers_count;
        
        ?>
        
        <div class="subscriber-wrap">
            
            <a href="http://www.twitter.com/<?php echo $twitter; ?>" class="subscriber-icon"><img src="<?php echo get_bloginfo( 'template_url' ); ?>/images/twitter.png" alt="Twitter" width="48" height="48" /></a>
            
            <span class="subscribes"><?php echo $count; ?></span>
            <span class="subscribes-text"><?php _e('Followers', 'mythemeshop'); ?></span>
                                        
        </div>
        
        <?php
        
        endif;
        
        ?>
        
        <?php
        
        if( !empty($rss) ):
        
        $rssurl = file_get_contents('http://feedburner.google.com/api/awareness/1.0/GetFeedData?uri=' . $rss);
        preg_match( '/circulation="(\d+)"/', $rssurl, $matches );
        if ( $matches[1] )
        $subscribers = $matches[1];
        else
        $subscribers = "0";
        
        ?>
        
        <div class="subscriber-wrap">
            
            <a href="<?php echo $rss; ?>" class="subscriber-icon"><img src="<?php echo get_bloginfo( 'template_url' ); ?>/images/rss.png" alt="RSS" width="48" height="48" /></a>
            
            <span class="subscribes"><?php echo $subscribers; ?></span>
            <span class="subscribes-text"><?php _e('Subscribers', 'mythemeshop'); ?></span>
                                        
        </div>
        
        <?php
        
        endif;
        
        ?>
        
        </div>
        
        <?php
        		
		echo $after_widget;
		
	}

}


add_action( 'widgets_init', 'mts_widget_subscribers' );
function mts_widget_subscribers() {
	register_widget('mts_Subscribers');
}
    
?>