<?php
/**
 * Template Name: Page Without Sidebar
 */
?>
<?php get_header(); ?>
<div id="content" class="hfeed nosidebar">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		<div class="page-single post_box">
			<article>
			<header>
				<div class="headline_area">
				<h2 class="entry-title"><?php the_title(); ?></h2>
				</div><!--.headline_area-->
			</header>
				<div class="format_text entry-content">
					<?php the_content(); ?>
					<?php wp_link_pages('before=<div class="pagination">&after=</div>'); ?>
				</div><!--.post-content .page-content -->
			</article>
		</div><!--#post-# .post-->
		<?php comments_template( '', true ); ?>
	<?php endwhile; ?>
</div><!--#content-->
<?php get_footer(); ?>