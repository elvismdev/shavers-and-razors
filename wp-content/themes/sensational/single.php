<?php $options = get_option('sensational'); ?>
<?php get_header(); ?>
<?php if ($options['mts_layout'] == 'cslayout' || $options['mts_layout'] == 'sclayout') { ?>
<?php } else { ?>
<aside>
<div id="sidebar-left" class="left-menu">
<?php if ( ! dynamic_sidebar( 'Left Sidebar' ) ) : ?>
<?php endif ?>
</div>
</aside>
<?php } ?>
<div id="content" class="hfeed">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		<article>
		<div id="post-<?php the_ID(); ?>" <?php post_class('post_box'); ?>>
			 <?php if ($options['mts_breadcrumb'] == '1') { ?>
			<div class="breadcrumb"><?php the_breadcrumb(); ?></div>
			<?php } ?>
			<header>
			<div class="headline_area">
				<h2 class="entry-title"><?php the_title(); ?></h2>
			<?php if($options['mts_headline_meta'] == '1') { ?>
				<div class="headline_meta">
				<p class="theauthor"><?php _e('By ', 'mythemeshop'); the_author_posts_link(); ?></p>
				<p class="themeta"><span class="thetime"><?php the_time('F j, Y'); ?></span><span class="thecategories"><?php the_category(' ') ?></span><span class="thecomments"><a href="<?php comments_link(); ?>"><?php comments_number('No comments','1 Comment','% Comments'); ?></a></span></p>
				</div>
			<?php } ?>
				</div><!--.headline_area-->
			</header>
		<?php if($options['mts_social_buttons'] == '1') { ?>
			<div class="shareit">
				<?php if($options['mts_twitter'] == '1') { ?>
						<!-- Twitter -->
						<span class="share-item twitterbtn">
						<a href="https://twitter.com/share" class="twitter-share-button" data-via="<?php echo $options['mts_twitter_username']; ?>">Tweet</a>
						</span>
				<?php } ?>
				<?php if($options['mts_gplus'] == '1') { ?>
						<!-- GPlus -->
						<span class="share-item gplusbtn">
						<g:plusone size="medium"></g:plusone>
						</span>
				<?php } ?>
				<?php if($options['mts_facebook'] == '1') { ?>
						<!-- Facebook -->
						<span class="share-item facebookbtn">
						<div id="fb-root"></div>
						<div class="fb-like" data-send="false" data-layout="button_count" data-width="150" data-show-faces="false"></div>
						</span>
				<?php } ?>
				<?php if($options['mts_linkedin'] == '1') { ?>
						<!--Linkedin -->
						<span class="share-item linkedinbtn">
						<script type="text/javascript" src="http://platform.linkedin.com/in.js"></script><script type="in/share" data-url="<?php the_permalink(); ?>" data-counter="right"></script>
						</span>
				<?php } ?>
				<?php if($options['mts_digg'] == '1') { ?>
						<!--Digg -->
						<span class="share-item diggbtn">
						<script type="text/javascript">
						(function() {
						var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
						s.type = 'text/javascript';
						s.async = true;
						s.src = 'http://widgets.digg.com/buttons.js';
						s1.parentNode.insertBefore(s, s1);
						})();
						</script>
						<a class="DiggThisButton DiggCompact"></a>
						</span>
				<?php } ?>
				<?php if($options['mts_stumble'] == '1') { ?>
						<!-- Stumble -->
						<span class="share-item stumblebtn">
						<su:badge layout="1"></su:badge>
						<script type="text/javascript"> 
						(function() { 
						var li = document.createElement('script'); li.type = 'text/javascript'; li.async = true; 
						li.src = window.location.protocol + '//platform.stumbleupon.com/1/widgets.js'; 
						var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(li, s); 
						})(); 
						</script>
						</span>
				<?php } ?>
				<?php if($options['mts_pinterest'] == '1') { ?>
						<!-- Pinterest -->
						<span class="share-item pinbtn">
						<div id="pinterest-pin" class="sprite-site-elements viral-button viral-button-pinterest right" onclick="void((function(){var e=document.createElement(&quot;script&quot;);e.setAttribute(&quot;type&quot;,&quot;text/javascript&quot;);e.setAttribute(&quot;charset&quot;,&quot;UTF-8&quot;);e.setAttribute(&quot;src&quot;,&quot;http://assets.pinterest.com/js/pinmarklet.js?r=&quot;+Math.random()*99999999);document.body.appendChild(e)})());"></div>
						</span>
				<?php } ?>
			</div>
		<?php } ?>
		<?php if ($options['mts_posttop_adcode'] != '') { ?>
				<?php 
				$toptime = $options['mts_posttop_adcode_time'];
				if (strcmp( date("Y-m-d", strtotime( "-$toptime day")), get_the_time("Y-m-d") ) >= 0) { ?>
				<div class="topad">
					<?php echo $options['mts_posttop_adcode']; ?>
				</div>
				<?php } ?>
			<?php } ?>
				<div class="format_text entry-content">
 <?php the_content(); ?>
 <?php wp_link_pages('before=<div class="pagination2">&after=</div>'); ?>
				</div><!--.format_text entry-content-->
					<?php if ($options['mts_postend_adcode'] != '') { ?>
				<?php 
				$endtime = $options['mts_postend_adcode_time'];
				if (strcmp( date("Y-m-d", strtotime( "-$endtime day")), get_the_time("Y-m-d") ) >= 0) { ?>
				<div class="bottomad">
					<?php echo $options['mts_postend_adcode'];?>
				</div>
					<?php } ?>
				<?php } ?>
	<?php if($options['mts_related_posts'] == '1') { ?>
			<div class="related-posts">
			<?php
				$categories = get_the_category($post->ID);
				if ($categories) {
				$category_ids = array();
				foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;

				$args=array(
					'category__in' => $category_ids,
					'post__not_in' => array($post->ID),
					'showposts'=>3, // Number of related posts that will be shown.
					'caller_get_posts'=>1
				);

				$my_query = new wp_query( $args );
				if( $my_query->have_posts() ) {
				echo '<h3>'.__('Related Posts','mythemeshop').'</h3><ul>';
				while( $my_query->have_posts() ) {
++$counter;
  if($counter == 3) {
    $postclass = 'last';
    $counter = 0;
  } else { $postclass = ''; }
				$my_query->the_post();?>

				<li class="<?php echo $postclass; ?>">
					<a rel="nofollow" class="relatedthumb" href="<?php the_permalink()?>" rel="bookmark" title="<?php the_title(); ?>"><span class="rthumb">
<?php if(has_post_thumbnail()): ?>
<?php the_post_thumbnail('related', 'title='); ?>
<?php else: ?>
<img src="<?php echo get_template_directory_uri(); ?>/images/relthumb.png" alt="<?php the_title(); ?>"  width='180' height='120' class="wp-post-image" />
<?php endif; ?>
</span><?php if (strlen($post->post_title) > 52) {
echo substr(the_title($before = '', $after = '', FALSE), 0, 52) . '...'; } else {
the_title();
} ?></a>
				</li>
				<?php
				}
				echo '</ul>';
				}
				}
				wp_reset_query();
			?>
		</div><!-- .related-posts -->
	<?php } ?>
	<?php if($options['mts_tags'] == '1') { ?>
			<div class="tags"><?php the_tags('<span class="tagtext">Tags:</span>','') ?></div>
	<?php } ?>
		</div><!-- #post-## -->
		</article>
	<?php if($options['mts_author_box'] == '1') { ?>
		<div class="postauthor">
			<?php if(function_exists('get_avatar')) { echo get_avatar( get_the_author_meta('email'), '80' );  } ?>
			<h4>Written by <a href="<?php the_author_meta( 'user_url' ); ?>" rel="nofollow"><?php the_author_meta( 'nickname' ); ?></a></h4>
			<p><?php the_author_meta('description') ?> 
				<div id="author-link">
					<p><?php _e('View all posts by: ', 'mythemeshop'); the_author_posts_link() ?></p>
				</div><!--#author-link-->
			</p><!--#author-description -->
		</div><!--.postauthor-->
		<?php } ?>
		<?php comments_template( '', true ); ?>
	<?php endwhile; /* end loop */ ?>
</div><!--#content-->
<?php get_sidebar(); ?>
<?php get_footer(); ?>