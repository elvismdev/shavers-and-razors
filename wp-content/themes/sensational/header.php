<!DOCTYPE html>
<?php $options = get_option('sensational'); ?>
<html class="no-js" dir="ltr" <?php language_attributes(); ?>>
<head>
	<meta charset="UTF-8">

	<title><?php wp_title(''); ?></title>

	<?php if ($options['mts_favicon'] != '') { ?>
	<link rel="icon" href="<?php echo $options['mts_favicon']; ?>" type="image/x-icon" />
	<?php } ?>

<!--iOS/android/handheld specific -->
	<link rel="apple-touch-icon" href="apple-touch-icon.png">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">

	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<?php if ($options['mts_title_font'] != '') { ?>
<?php if ($options['mts_title_font'] == 'Bree Serif') { ?>
<?php } else { ?>
	<link href="http://fonts.googleapis.com/css?family=<?php echo $options['mts_title_font']; ?>" rel="stylesheet" type="text/css">
<style type="text/css">
#header_area, .headline_area h1, .headline_area h2, h1, h2, h3, h4, h5, h6, .sidebar h3, #sidebar-left, #footer_setup h3, .copyrights {
font-family: '<?php echo $options['mts_title_font']; ?>', Arial, sans-serif;
}
</style>
<?php } ?>
<?php } ?>
<?php if ($options['mts_content_font'] != '') { ?>
<link href="http://fonts.googleapis.com/css?family=<?php echo $options['mts_content_font']; ?>" rel="stylesheet" type="text/css">
<?php } else { ?>
<link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">
<?php } ?>


	<?php wp_enqueue_script("jquery"); ?>
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
	<?php wp_head(); ?>

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/modernizr.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/customscript.js" type="text/javascript"></script>

<?php if($options['mts_lightbox'] == '1') { ?>
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/prettyPhoto.css" type="text/css" media="screen" charset="utf-8" />
	<script src="<?php bloginfo('template_directory'); ?>/js/jquery.prettyPhoto.js"></script>
	<script type="text/javascript">
	jQuery(document).ready(function($) {
   	$("a[href$='.jpg'], a[href$='.jpeg'], a[href$='.gif'], a[href$='.png']").prettyPhoto({
	slideshow: 5000, /* false OR interval time in ms */
	autoplay_slideshow: false, /* true/false */
	animationSpeed: 'normal', /* fast/slow/normal */
	padding: 40, /* padding for each side of the picture */
        opacity: 0.35, /* Value betwee 0 and 1 */
	showTitle: true, /* true/false */
	social_tools: false
	});
	})
	</script>
 <?php } ?>

<?php if($options['mts_featured_slider'] == '1') { ?>
<?php if( is_home() ) { ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/flexslider.css" type="text/css">
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider-min.js"></script>
	<script type="text/javascript">
	$(window).load(function() {
    $('.flexslider').flexslider({
          animation: "fade",
		  pauseOnHover: true,
          controlsContainer: ".flex-container"
    });
	});
	</script>
<?php } ?>
<?php } ?>

<?php if($options['mts_floating_leftside'] == '1') { ?>
	<script type="text/javascript">
    $(window).scroll(function() {
        $('#sidebar-left').animate({ top: $(window).scrollTop() + "px" }, { queue: false, duration: 500}, "fast");
    });
	</script>
<?php } ?>
<style type="text/css">
<?php if ($options['mts_primary_color'] != '') { ?>
			.left-menu .cat-item a, #commentform input#submit, .reply a{
			background:<?php echo $options['mts_primary_color']; ?>;
			}
<?php } ?>
<?php if ($options['mts_secondary_color'] != '') { ?>
			a, .related-posts a:hover  {
			color:<?php echo $options['mts_secondary_color']; ?>;
			}
			.currenttext, .pagination2 a, .inactive:hover, .readmore, .mts-subscribe input[type="submit"] {
			background-color:<?php echo $options['mts_secondary_color']; ?>;
			}
			.post_box, .postauthor {
			border-bottom: 5px solid <?php echo $options['mts_secondary_color']; ?>;
			}
			.current-menu-item {
			border-top: 4px solid <?php echo $options['mts_secondary_color']; ?>;
			}
<?php } ?>
<?php if ($options['mts_logo'] != '') { ?>
			#header h1, #header h2 {
			text-indent: -999em;
			min-width:138px;
			margin: 0;
			margin-bottom: 10px;
			}
			#header h1 a, #header h2 a{
			background: url(<?php echo $options['mts_logo']; ?>) no-repeat;
			min-width: 138px;
			display: block;
			min-height: 35px;
			line-height: 28px;
			}
		<?php } ?>
body {
<?php if ($options['mts_bg_color'] != '') { ?>
background-color:<?php echo $options['mts_bg_color']; ?>;
<?php } ?>
}
<?php if ($options['mts_bg_pattern_upload'] != '') { ?>
body {
background-image: url(<?php echo $options['mts_bg_pattern_upload']; ?>);
}
<?php } else { ?>
<?php if ($options['mts_bg_pattern'] == 'nobg') { ?>
#content_area .page {
border-color:#fff;
background: transparent;
}
#content_area {
padding-top:0;
padding-bottom:0;
}
#content_area .page {
border: 0;
background: transparent;
}
<?php } else { ?>
<?php if ($options['mts_bg_pattern'] != '') { ?>
body {
background-image: url(<?php echo get_template_directory_uri(); ?>/images/<?php echo $options['mts_bg_pattern']; ?>.png);
}
<?php } ?>
<?php } ?>
<?php } ?>

<?php if($options['mts_author_comment'] == '1') { ?>
.bypostauthor {
background: #F7FBFB url(<?php echo get_template_directory_uri(); ?>/images/author.png) right 0 no-repeat!important;
border: 1px solid #87AEC4!important;
}
<?php } ?>
<?php if($options['mts_floating_header'] == '1') { ?>
#header_area {
position: fixed;
top: 0;
left: 0;
z-index: 50;
border-bottom: 1px solid #EEE;
opacity: 0.9;
}
#content_area {
margin-top: 45px;
}
<?php } ?>
<?php if($options['mts_floating_leftside'] == '1') { ?>
#content {
left: 204px;
}
#sidebar-left {
position: relative;
}
#content_box {
overflow:hidden;
}
<?php } ?>
<?php if ($options['mts_content_font'] != '') { ?>
body {
font-family: '<?php echo $options['mts_content_font']; ?>', Arial, sans-serif;
}
<?php } ?>
<?php if ($options['mts_title_size'] != '') { ?>
.headline_area h1, .headline_area h2 {
font-size:<?php echo $options['mts_title_size']; ?>;
}
<?php } ?>
<?php if ($options['mts_body_size'] != '') { ?>
body {
font-size:<?php echo $options['mts_body_size']; ?>;
}
<?php } ?>
<?php if($options['mts_image_border'] == '1') { ?>
.format_text img,
.format_text img.left,
.format_text img.alignleft,
.format_text img.right,
.format_text img.alignright,
.format_text img.center,
.format_text img.aligncenter,
.format_text img.alignnone {
border: 1px solid lightGrey;
padding: 5px;
background: white;
border-top-left-radius: 4px;
border-top-right-radius: 4px;
border-bottom-right-radius: 4px;
border-bottom-left-radius: 4px;
}
.format_text img:hover,
.format_text img.left:hover,
.format_text img.alignleft:hover,
.format_text img.right:hover,
.format_text img.alignright:hover,
.format_text img.center:hover,
.format_text img.aligncenter:hover,
.format_text img.alignnone:hover {
background-color: #D8D9DA;
border-color: #BCBCBC;
}
<?php } ?>
<?php if ($options['mts_layout'] == 'scclayout') { ?>
#content {
float: right;
border-left: 1px #F2F2F2 solid;
border-right:0;
}
.post_box {
padding: 20px 0 40px 20px;
}
#sidebar-left {
float:right;
margin: 30px 0 0 30px;
}
#sidebars {
float: left;
}
.left-menu li {
text-align: left;
}
.postauthor {
padding-right: 0;
padding-left: 25px;
}
.comments_intro {
padding-left: 25px;
}
.commentlist li {
border-left:0;
border-right: 1px solid #DDD;
}
.commentlist .children li {
margin-right:0;
}
#respond h3 {
margin-left: 20px;
}
#commentform {
margin-left: 20px;
}
/*-----------------------------------------[ 1024 ]-------------------------------------*/
@media screen and (max-width:1024px){
#content {
padding-left:3%;
padding-right:3%;
border-left:0;
}
#sidebars {
width: 25%;
padding-right:0;
}
.ad-125 li {
margin-left:0!important;
}
#s {
width: 70%;
}
}
/*-----------------------------------------[ 768 ]-------------------------------------*/
@media screen and (max-width: 768px) {
#content {
width: 94%
}
#sidebars {
width: 100%;
}
}
<?php } ?>
<?php if ($options['mts_layout'] == 'cslayout') { ?>
#content {
width: 57.5%;
padding-right: 4%;
}
#sidebars {
width: 29%;
}
.page {
width: 1030px;
}
#content {
padding-left: 35px;
}
.footer_items_1, .footer_items_2, .footer_items_3, .footer_items_4 {
width: 32%;
}
.footer_items_3 {
margin-right: 0;
}
.footer_items_4 {
float:none;
}
/*-----------------------------------------[ 1024 ]-------------------------------------*/
@media screen and (max-width: 1024px) {
.page {
width: 95%;
}
.footer_items_4 {
float: left;
}
.footer_items_3 {
margin-right: 20px;
}
}
/*-----------------------------------------[ 768 ]-------------------------------------*/
@media screen and (max-width: 768px) {
#content {
width: 95%;
}
#sidebars {
width: 95%;
}
}
/*-----------------------------------------[ 480 ]-------------------------------------*/
@media screen and (max-width: 480px) {
.sidebar {
padding-left: 8%;
}
}
/*-----------------------------------------[ 320 ]-------------------------------------*/
@media screen and (max-width: 320px) {
#content {
padding-left: 20px;
}
}
<?php } ?>
<?php if ($options['mts_layout'] == 'sclayout') { ?>
.page {
width: 1030px;
}
#content {
width: 57.5%;
float: right;
border-left: 1px #F2F2F2 solid;
border-right:0;
padding-left: 3%;
padding-right: 3%;
}
#sidebars {
float: left;
width: 29%;
}
.postauthor {
margin-right: 20px;
}
#comments {
margin-right: 20px;
}
.commentlist li {
border: 1px solid #DDD;
}
.commentlist .children li {
margin-right:0;
}
.footer_items_1, .footer_items_2, .footer_items_3, .footer_items_4 {
width: 32%;
}
.footer_items_3 {
margin-right: 0;
}
.footer_items_4 {
float:none;
}
/*-----------------------------------------[ 1024 ]-------------------------------------*/
@media screen and (max-width: 1024px) {
.page {
width: 95%;
}
.footer_items_4 {
float: left;
}
.footer_items_3 {
margin-right: 20px;
}
#sidebars {
float: left;
width: 27%;
}
}
/*-----------------------------------------[ 768 ]-------------------------------------*/
@media screen and (max-width: 768px) {
#content {
width:95%;
}
#sidebars {
width: 100%;
}
}
/*-----------------------------------------[ 320 ]-------------------------------------*/
@media screen and (max-width: 320px) {
.sidebar {
padding-left: 0;
}
}
<?php } ?>
<?php echo $options['mts_custom_css']; ?>
</style>

<?php echo $options['mts_header_code']; ?>

</head>

<?php flush(); ?>

<body <?php body_class('main'); ?>>
	<header id="header_area">
		<div class="page">
			<div id="header">
				<?php if( is_front_page() || is_home() || is_404() ) { ?>
		<div class="text-logo">
                    <h1 id="logo">
						<a href="<?php echo home_url(); ?>"><?php bloginfo( 'name' ); ?></a>
					</h1>
		</div>
                    <!-- END #logo -->
				<?php } else { ?>
		<div class="text-logo">
                    <h2 id="logo">
						<a href="<?php echo home_url(); ?>"><?php bloginfo( 'name' ); ?></a>
					</h2>
		</div>
                    <!-- END #logo -->
				<?php } ?>
			</div><!--#header-->
<nav class="main-navigation">
		<?php if ( has_nav_menu( 'header-menu' ) ) { ?>
                        <?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'menu_class' => 'menu', 'container' => '' ) ); ?>
                        <?php } else { ?>
                        <ul class="menu">
							<li class="home-tab"><a href="<?php echo home_url(); ?>">Home</a></li>
                            <?php wp_list_pages('title_li='); ?>
                        </ul>
        <?php } ?><!--#nav-primary-->
</nav>
		</div><!--.page-->
	</header><!--#header_area-->
	<div id="content_area">
		<div class="page">
			<div id="content_box">