<?php $options = get_option('sensational'); ?>
	</div><!--.page-->
	</div><!--#content_box-->
	</div><!--.container-->
<footer>
	<div id="footer_area" class="full_width">
		<div class="page">
			<div id="footer">
			<?php widgetized_footer(); ?>
			<div id="footermenu">
              <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
  			</div>
<div class="copyrights">
<?php if ($options['mts_copyrights'] != '') { ?>
<?php echo $options['mts_copyrights']; ?>
<?php } else { ?>
<span>&copy; <?php echo date("Y") ?> <a href="<?php echo home_url(); ?>/" title="<?php bloginfo('description'); ?>"><?php bloginfo('name'); ?></a>. <?php _e('All Rights Reserved.', 'mythemeshop'); ?></span>
			<!-- <span>Theme by <a href="http://mythemeshop.com">MyThemeShop</a>.</span> -->
<?php } ?>
			</div>
			</div><!--#footer-->
		</div><!--.page-->
	</div><!--#footer_area-->
</footer>
<?php wp_footer(); ?>
</script>
<!--Twitter Button Script------>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
<script type="text/javascript">
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>
<!--Facebook Like Button Script------>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=136911316406581";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script> 
<!--Google Analytics Script------>
<?php if ($options['mts_analytics_code'] != '') { ?>
<?php echo $options['mts_analytics_code']; ?>
<?php } ?>
</body>
</html>