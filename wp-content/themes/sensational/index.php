<?php $options = get_option('sensational'); ?>
<?php get_header(); ?>
<?php if ($options['mts_layout'] == 'cslayout' || $options['mts_layout'] == 'sclayout') { ?>
<?php } else { ?>
<div id="sidebar-left" class="left-menu">
<?php if ( ! dynamic_sidebar( 'Left Sidebar' ) ) : ?>
<?php endif ?>
</div>
<?php } ?>
	<div id="content" class="hfeed homepage">
<?php if (is_home() && !is_paged()) { ?>
	<?php if($options['mts_featured_slider'] == '1') { ?>
	<div class="flex-container">
  <div class="flexslider">
    <ul class="slides">
	<?php
$my_query = new WP_Query('cat='.$options['mts_featured_cat'].'&posts_per_page=3');
while ($my_query->have_posts()) : $my_query->the_post();
?>
      <li><a href="<?php the_permalink() ?>">
        <?php the_post_thumbnail('featured',array('title' => '')); ?>
		<p class="flex-caption"><?php the_title(); ?></p>
      </li>
   <?php endwhile; ?> </ul>
  </div>
</div>
<?php } ?>
<?php } ?>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<article class="article">
			<div class="post-single post_box">
				<header>
				<div class="headline_area">
				<h2 class="entry-title"><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
			<?php if($options['mts_headline_meta'] == '1') { ?>
				<div class="headline_meta">
				<p class="theauthor"><?php _e('By ', 'mythemeshop'); the_author_posts_link(); ?></p>
				<p class="themeta"><span class="thetime"><?php the_time('F j, Y'); ?></span><span class="thecategories"><?php the_category(' ') ?></span><span class="thecomments"><a href="<?php comments_link(); ?>" rel="nofollow"><?php comments_number('No comments','1 Comment','% Comments'); ?></a></span></p>
				</div>
			<?php } ?>
				</div><!--.headline_area-->
				</header>
				<?php if($options['mts_thumbnails'] == '1') { ?>
				<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="nofollow"><?php if ( has_post_thumbnail() ) { echo '<div class="featured-thumbnail">'; the_post_thumbnail('thumbnail', array('title' => '')); echo '</div>'; } ?></a>
				<?php } ?>
				<div class="format_text entry-content">
					<?php the_excerpt(); ?>
					<p><a href="<?php the_permalink() ?>" class="readmore" rel="nofollow">Read More...</a></p>
				</div>
			</div><!--.post-single-->
			</article>
		<?php endwhile; else: ?>
			<div class="no-results">
				<p><strong><?php _e('There has been an error.', 'mythemeshop'); ?></strong></p>
				<p><?php _e('We apologize for any inconvenience, please hit back on your browser or use the search form below.', 'mythemeshop'); ?></p>
				<?php get_search_form(); ?>
			</div><!--noResults-->
		<?php endif; ?>
<?php if ($options['mts_pagenavigation'] == '1') { ?>
<?php pagination($additional_loop->max_num_pages);?>
<?php } else { ?>
<div class="pnavigation2">
<div class="nav-previous"><?php next_posts_link( __( '&larr; '.'Older posts', 'mythemeshop' ) ); ?></div>
<div class="nav-next"><?php previous_posts_link( __( 'Newer posts'.' &rarr;', 'mythemeshop' ) ); ?></div>
</div>
<?php } ?>
	</div><!--#content-->
<?php get_sidebar(); ?>
<?php get_footer(); ?>