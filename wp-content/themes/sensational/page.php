<?php $options = get_option('sensational'); ?>
<?php get_header(); ?>
<?php if ($options['mts_layout'] == 'cslayout' || $options['mts_layout'] == 'sclayout') { ?>
<?php } else { ?>
<div id="sidebar-left" class="left-menu">
<?php if ( ! dynamic_sidebar( 'Left Sidebar' ) ) : ?>
<?php endif ?>
</div>
<?php } ?>
<div id="content" class="hfeed">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		<div class="page-single post_box">
			<?php if ($options['mts_breadcrumb'] == '1') { ?>
			<div class="breadcrumb"><?php the_breadcrumb(); ?></div>
			<?php } ?>
			<article>
			<header>
				<div class="headline_area">
				<h2 class="entry-title"><?php the_title(); ?></h2>
				</div><!--.headline_area-->
			</header>
				<div class="format_text entry-content">
					<?php the_content(); ?>
					<?php wp_link_pages('before=<div class="pagination">&after=</div>'); ?>
				</div><!--.post-content .page-content -->
			</article>
		</div><!--#post-# .post-->
		<?php comments_template( '', true ); ?>
	<?php endwhile; ?>
</div><!--#content-->
<?php get_sidebar(); ?>
<?php get_footer(); ?>